dhcpy6d (1.2.3-1) unstable; urgency=medium

  * Import new upstream bugfix release 1.2.3.
    + Removes mentioning of unrelated software by same author from LICENSE
      file.
    + Fix invalid hostname due to wrong quoting.

 -- Axel Beckert <abe@debian.org>  Thu, 02 Mar 2023 01:07:36 +0100

dhcpy6d (1.2.2-1) unstable; urgency=medium

  * Update debian/watch to follow Github site changes.
  * Import new upstream release 1.2.2.
  * Replace dependency on transitional package lsb-base with a dependency
    on sysvinit-utils (>= 3.05-4~).
  * Declare compliance with Debian Policy 4.6.2. No changes needed.
  * Drop no more needed debian/source/lintian-overrides.
  * Bump copyright years in debian/copyright. Thanks Lintian!

 -- Axel Beckert <abe@debian.org>  Sun, 12 Feb 2023 21:22:55 +0100

dhcpy6d (1.0.7-1) unstable; urgency=medium

  * Import new upstream bugfix release 1.0.7.
    + Fixes PXE support broken since 1.0.3. (Closes: #995484)
  * Declare compliance with Debian Policy 4.6.0. (No changes required.)
  * Add lintian override for missing-build-dependency-for-dh-addon false
    positive. (See lintian bug report #996089)
  * Replace "which" with "command -v" in postrm. *sigh*
  * Rename all debian/dhcpy6d.<something> files to just debian/<something>
    for the sake of simplicity.

 -- Axel Beckert <abe@debian.org>  Mon, 11 Oct 2021 04:12:36 +0200

dhcpy6d (1.0.5-1) unstable; urgency=medium

  * Import new upstream bugfix release 1.0.5.
  * Update Homepage header, debian/{copyright,watch,upstream}, etc. to new
    upstream homepage URL. Also update upstream's e-mail address.

 -- Axel Beckert <abe@debian.org>  Sun, 15 Aug 2021 16:23:43 +0200

dhcpy6d (1.0.3-1) unstable; urgency=medium

  * Import new upstream release 1.0.3.
  * Remove Henri (upstream) from Uploaders since Debian's dhcpy6d package
    is no more maintained in the upstream repository. He's fine with that.

 -- Axel Beckert <abe@debian.org>  Fri, 25 Dec 2020 00:10:46 +0100

dhcpy6d (1.0.2-1) unstable; urgency=low

  * Prepare separating Debian packaging from upstream repo:
    + Update Vcs-* headers to point to Salsa.
    + Add debian/upstream/metadata file.
    + Generate pure Debian git history via "gbp import-dscs --debsnap",
      merge it with upstream's git history up to "debian/0.4.3-1".
    + Add a debian/gbp.conf.
  * Import upstream release 1.0.2 and parts of upstream's Python 3 based
    packaging. (Closes: #936391)
    + Upstream moved init.d script as well as systemd service file under
      debian/. The latter differs for /etc/default/dhcpy6d integration.
    + Minor wording fixes in the 0.4.2-1 changelog entry.
    + Switch packaging to Python 3 (only).
    + Bump debhelper build-dependency (but not compat) to 12.1.1.
    + Depend on lsb-base.
    + Move python*-dnspython from Suggests to Depends.
  * Fix upstream packaging as needed:
    + Drop X-Python*-Version headers.
    + Fix broken setup.py (which creates files during clean target) by
      creating debian/clean with an according entry.
    + Also add PKG-INFO, SOURCES.txt, dependency_links.txt, top_level.txt
      and dhcpy6d.egg-info to debian/clean.
    + Drop debian/rules lines related to hardcoded Python 3.7 version.
    + Merge usr/share/dhcpy6d/ and usr/share/dhcpy6d/default entries in
      dhcpy6d.dirs.
    + Drop var/lib and var/log from dhcpy6d.dirs.
    + Do not install log file.
    + Add missing ${misc:Pre-Depends} for --skip-systemd-native.
    + Add missing build-dependency on python3-setuptools.
  * Bump debhelper compatibility level to 13.
    + Build-depend on "debhelper-compat (= 13)" to replace debian/compat.
    + Drop build-dependency on dh-systemd.
    + Drop "--with systemd" from debian/rules.
  * Remove trailing whitespace and reduce all double blank lines between
    old debian/changelog entries.
  * debian/copyright: Use HTTPS for format URL.
  * Set "Rules-Requires-Root: binary-targets". (dh_python3 fails with
    "no".)
  * debian/rules: Drop unneeded "get-orig-source" target. Also fixes
    Lintian warning debian-rules-parses-dpkg-parsechangelog.
  * Declare compliance with Debian Policy 4.5.1. (No changes required.)
  * Bump debian/watch version from 3 to 4. Thanks Lintian!

 -- Axel Beckert <abe@debian.org>  Mon, 14 Dec 2020 06:25:15 +0100

dhcpy6d (0.4.3-1) unstable; urgency=low

  [ Henri Wahl ]
  * New upstream release
    + Added autocommit to MySQL
    + Fixed fixed addresses
    + Some optimization in tidy-up-thread
    + Small fixes

  [ Axel Beckert ]
  * Fix typo found by Lintian in package description.
  * Switch Vcs-Git header from git:// to https:// and add "-b debian".
  * Switch Homepage header from http:// to https://.
  * Declare compliance with Debian Policy 3.9.8. (No changes needed.)

 -- Axel Beckert <abe@debian.org>  Tue, 28 Jun 2016 21:12:53 +0200

dhcpy6d (0.4.3~dev1-1) unstable; urgency=medium

  [ Henri Wahl ]
  * New upstream snapshot
    + removed client FQDN in log file

  [ Axel Beckert ]
  * Merge adduser and usermod calls in debian/dhcpy6d.postinst. Fixes
    false positive lintian warning
    maintainer-script-should-not-use-adduser-system-without-home.
  * Bump debhelper compatibility to 9 as recommended nowadays.
    + Update versioned debhelper build-dependency accordingly.

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Fri, 21 Aug 2015 12:30:00 +0200

dhcpy6d (0.4.2-1) unstable; urgency=medium

  * New upstream snapshot
    + fixed usage of fixed addresses in dhcpy6d-clients.conf
    + fixed dns_update() to update default class clients too
    + show warning if deprecated prefix_length is used in address definitions
    + set socket to non-blocking to avoid freeze
    + increase MAC/LLIP cache time from 30s to 300s because of laggy clients
    + removed useless prefix length
    + retry query on MySQL reconnect bugfix

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Tue, 18 Aug 2015 16:00:00 +0200

dhcpy6d (0.4.1-1) unstable; urgency=medium

  [ Henri Wahl ]
  * New upstream release
    + VLAN definitions now really work
    + several code cleaned
    + Removes unnecessary executable bits (see #769006) → reinstantiate
      debian/dhcpy6d.logrotate as symlink

  [ Axel Beckert ]
  * Fix postinst script to not expect preinst parameters (Closes: #768974)

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Tue, 17 Mar 2015 08:50:00 +0100

dhcpy6d (0.4-2) unstable; urgency=medium

  * Handle /etc/default/dhcpy6d with ucf. (Closes: #767817)
    + Install file to /usr/share/dhcpy6d/default/dhcpy6d instead, remove
      symlink debian/dhcpy6d.default, add debian/dhcpy6d.install.
    + Depend on ucf.
  * Install volatile.sqlite into /usr/share/dhcpy6d/ and copy it to
    /var/lib/dhcpy6d/volatile.sqlite during postinst only if it doesn't
    yet exist. Remove it upon purge. (Closes: #768989)
  * Both fixes above together also remove unnecessary executable bits.
    (Else the fix for #767817 newly introduces the lintian warning
    executable-not-elf-or-script; closes: #769006)
  * Additionally replace symlink debian/dhcpy6d.logrotate with a patched
    copy of etc/logrotate.d/dhcpy6d to remove the executable bit also
    there. (Fixes another facet of #769006)

 -- Axel Beckert <abe@debian.org>  Thu, 13 Nov 2014 12:39:09 +0100

dhcpy6d (0.4-1) unstable; urgency=low

  [ Henri Wahl ]
  * New upstream release
    + new options: log_mac_llip, cache_mac_llip (avoids cache poisoning)

  [ Axel Beckert ]
  * Add get-orig-source target to debian/rules for easier snapshot
    packaging.
  * Depend on ${python:Depends} instead of a hardcoded python (>= 2.6).
  * Add "Pre-Depends: dpkg (>= 1.16.5) for "start-stop-daemon --no-close"
  * Drop dependency on iproute/iproute2 as /sbin/ip is no more used.

 -- Axel Beckert <abe@debian.org>  Wed, 22 Oct 2014 21:03:56 +0200

dhcpy6d (0.3.99+git2014.09.18-1) unstable; urgency=medium

  * New upstream release candidate + snapshot
    + allow VLAN interface definitions
    + check if used interfaces exist
    + improved usability with more clear mesages if there are
      configuration errors
    + full man pages dhcpy6d.8 and dhcpy6d.conf.5 added
    + added command line argument --generate-duid for DUID generation at setup

  [ Henri Wahl ]
  * Append generated DUID to /etc/default/dhcpy6d if not yet present
  * Added command line arguments --really-do-it and --duid to be
    configured in /etc/defaults/dhcpy6d

  [ Axel Beckert ]
  * Switch section from "utils" to "net" like most other DHCP servers.
  * Update debian/source/options to follow upstream directory name changes
  * Bump Standards-Version to 3.9.6 (no changes)

 -- Axel Beckert <abe@debian.org>  Thu, 02 Oct 2014 18:25:44 +0200

dhcpy6d (0.3+git2014.07.23-1) unstable; urgency=medium

  * New upstream snapshot.
    + Man pages moved from Debian to Upstream
    + Don't ship man pages installed to /usr/share/doc/dhcpy6d/
  * Delete dhcpy6d's log files upon package purge.
  * Add missing dependency on iproute2 or iproute. Thanks Henri!
  * Complete the switch from now deprecated python-support to dh_python2.
    + Only debian/control changes. (debian/rules was fine already.)
    + Fixes lintian warning build-depends-on-obsolete-package.

 -- Axel Beckert <abe@debian.org>  Thu, 24 Jul 2014 14:27:31 +0200

dhcpy6d (0.3+git2014.03.21-1) unstable; urgency=low

  * New upstream snapshot
  * First upload to Debian (Closes: #715010)
  * Switch back to non-native packaging
  * Set myself as primary package maintainer
  * Switch to source format "3.0 (quilt)".
    + Remove now obsolete README.source
  * Drop unnecessary build-dependency on quilt. Fixes lintian warning
    quilt-build-dep-but-no-series-file.
  * Add machine-readable debian/copyright. Fixes lintian warning
    no-debian-copyright.
  * Add "set -e" to postinst script to bail out on any error.
  * Move adduser from Suggests to Depends. Used in the postinst script.
  * Don't ship additional LICENSE file installed by upstream. Fixes
    lintian warning extra-license-file.
  * Add a debian/watch file. Fixes lintian warning
    debian-watch-file-is-missing.
  * Use short description from GitHub as short description.
  * Don't ship empty log file, create it at install time. Fixes lintian
    warning file-in-unusual-dir.
  * Add minimal man page with pointer to online documentation. Fixes
    lintian warning binary-without-manpage.
  * Also fix the following lintian warnings:
    + maintainer-address-malformed
    + maintainer-also-in-uploaders
    + no-standards-version-field
    + maintainer-script-lacks-debhelper-token
    + debhelper-but-no-misc-depends
    + description-starts-with-package-name
    + description-synopsis-might-not-be-phrased-properly
    + description-too-long (refers to first line)
    + extended-description-is-empty
  * Apply wrap-and-sort

 -- Axel Beckert <abe@debian.org>  Wed, 21 May 2014 14:25:27 +0200

dhcpy6d (0.3) unstable; urgency=low

  * New upstream
    - running as non-root user/group dhcpy6d
    - deb improvements
    - rpm improvements

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Mon, 29 Jul 2013 13:14:00 +0200

dhcpy6d (0.2-1) unstable; urgency=low

  * New upstream
    - next fix in 'range' lease storage, getting more robust
    - better logging

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Fri, 31 May 2013 14:40:00 +0200

dhcpy6d (0.1.5-1) unstable; urgency=low

  * New upstream
    - fixed race condition in 'range' lease storage

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Thu, 23 May 2013 11:00:00 +0200

dhcpy6d (0.1.4.1-1) unstable; urgency=low

  * New upstream
    - fixed lease storage bug

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Sat, 18 May 2013 00:50:00 +0200

dhcpy6d (0.1.4-1) unstable; urgency=low

  * New upstream
    - fixed advertised address handling for categories 'range' and 'random'

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Fri, 17 May 2013 14:50:00 +0200

dhcpy6d (0.1.3-1) unstable; urgency=low

  * New upstream
    - added domain_search_list option
    - fixed case-sensitive MAC address config

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Mon, 06 May 2013 14:50:00 +0200

dhcpy6d (0.1.2-1) unstable; urgency=low

  * New upstream
    - fixed multiple addresses renew bug

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Tue, 19 Mar 2013 9:02:00 +0200

dhcpy6d (0.1.1-1) unstable; urgency=low

  * New upstream
    - reverted to Handler.finish()

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Tue, 15 Jan 2013 07:35:00 +0200

dhcpy6d (0.1-1) unstable; urgency=low

  * New upstream
    - inital stable release

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Wed, 11 Jan 2013 14:10:00 +0200

dhcpy6d (20130111-1) unstable; urgency=low

  * New upstream
    - more polishing for rpm packaging support

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Wed, 11 Jan 2013 13:18:00 +0200

dhcpy6d (20130109-1) unstable; urgency=low

  * New upstream
    - polishing packaging support

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Wed, 09 Jan 2013 14:16:00 +0200

dhcpy6d (20121221-1) unstable; urgency=low

  * New upstream
    - finished Debian support

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Thu, 21 Dec 2012 11:25:00 +0200

dhcpy6d (20121220-1) unstable; urgency=low

  * New upstream
    - testing Debian support

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Thu, 20 Dec 2012 11:25:00 +0200

dhcpy6d (20121219-1) unstable; urgency=low

  * New upstream
    - testing Debian support

 -- Henri Wahl <h.wahl@ifw-dresden.de>  Wed, 19 Dec 2012 11:25:00 +0200
